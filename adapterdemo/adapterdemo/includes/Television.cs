using System;
using adapterdemo.includes.AVDevices;

namespace adapterdemo.includes
{
    /// <summary>
    /// Class <c>Television</c> is the main class in this demo. It is the client class that's designed to talk to an <c>IAvInput</c>
    /// </summary>
    public class Television
    {
        /// <summary>
        /// Holds a reference of the connected <c>IAvInput</c> device.
        /// </summary>
        private IAvInput _avInput;
        
        /// <summary>
        /// Method emulates a remote-controll 'Play button'. This calls the <c>Play()</c> method on the connected <c>IAvInput _avInput</c>.
        /// </summary>
        /// <returns>IAvInput</returns>
        public IAvInput PlayButton()
        {
            if (_avInput == null)
            {
                Console.WriteLine("[Television] No input device connected.");
                return null;
            }

            Console.WriteLine("[Television] The play button was pressed.");
            this._avInput.Play();
            return this._avInput;
        }

        /// <summary>
        /// Changes the <c>IAvInput _avInput</c>, basically a setter.
        /// </summary>
        /// <param name="avInput"></param>
        public void ConnectAvInput(IAvInput avInput)
        {
            Console.WriteLine("[Television] A new avInput is connected.");
            this._avInput = avInput;
        }
    }
}