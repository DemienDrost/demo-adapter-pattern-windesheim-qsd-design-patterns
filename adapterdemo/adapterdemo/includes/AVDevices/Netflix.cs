using System;

namespace adapterdemo.includes.AVDevices
{
    /// <summary>
    /// Class <c>Netflix</c> implements <c>IStreamingService</c> and acts as a data-source.
    /// </summary>
    public class Netflix : ISteamingService
    {
        /// <summary>
        /// Method <c>PlayStream()</c> prints 'Netflixy' content to the console.
        /// </summary>
        public void PlayStream()
        {
            Console.WriteLine("-------------------------------");
            Console.WriteLine("    _   __     __  _______     ");
            Console.WriteLine("   / | / /__  / /_/ __/ (_)  __");
            Console.WriteLine("  /  |/ / _ \\/ __/ /_/ / / |/_/");
            Console.WriteLine(" / /|  /  __/ /_/ __/ / />  <  ");
            Console.WriteLine("/_/ |_/\\___/\\__/_/ /_/_/_/|_|  ");
            Console.WriteLine("-------------------------------");
        }
    }
}