namespace adapterdemo.includes.AVDevices
 {
     /// <summary>
     /// Class <c>AppleTv</c> uses the Class Adapter Design Pattern to match the interfaces between an <c>IAvInput</c> and a <c>Netflix</c> instance.
     /// </summary>
     public class AppleTv : Netflix, IAvInput
     {
         /// <summary>
         /// Method <c>Play()</c> remaps the <c>Play()</c> method from an <c>IAvInput</c> to the <c>PlayStream()</c> method from a <c>Netflix</c> class.
         /// </summary>
         public void Play()
         {
             PlayStream();
         }
     }
 }