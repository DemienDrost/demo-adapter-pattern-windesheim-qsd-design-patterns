using System;

namespace adapterdemo.includes.AVDevices
{
    /// <summary>
    /// Class <c>Chromecast</c> uses the Object Adapter Design Pattern to match the interfaces between an <c>IAvInput</c> and an <c>ISteamingService</c> instance.
    /// </summary>
    public class Chromecast : IAvInput
    {
        /// <summary>
        /// Holds a reference of <c>ISteamingService</c>, this object is the object we're adapting to. This is called the adaptee. 
        /// </summary>
        private ISteamingService _service;

        /// <summary>
        /// The constructor method saves the reference object. 
        /// </summary>
        public Chromecast(ISteamingService service)
        {
            this._service = service;
        }
        
        /// <summary>
        /// The <c>Play()</c> method adapts the <c>Play()</c> method to the adaptees <c>PlayStream()</c> method.
        /// </summary>
        public void Play()
        {
            Console.WriteLine("[Chromecast (Adapter)] Play method called, remapping to PlayStream");
            this._service.PlayStream();
        }
    }
}