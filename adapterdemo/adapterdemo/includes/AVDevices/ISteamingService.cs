namespace adapterdemo.includes.AVDevices
{
    /// <summary>
    /// Interface to dictate the methods of a streamingservice
    /// </summary>
    public interface ISteamingService
    {
        void PlayStream();
    }
}