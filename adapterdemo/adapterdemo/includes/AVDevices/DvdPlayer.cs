using System;

namespace adapterdemo.includes.AVDevices
{
    /// <summary>
    /// Acts as an input device for the television.
    /// </summary>
    public class DvdPlayer : IAvInput
    {
        /// <summary>
        /// Method <c>Play()</c> prints 'DVD' content to the console.
        /// </summary>
        public void Play()
        {
            Console.WriteLine("--------------------------");
            Console.WriteLine(" ______            ______");
            Console.WriteLine("(  __  \\ |\\     /|(  __  \\");
            Console.WriteLine("| (  \\  )| )   ( || (  \\  )");
            Console.WriteLine("| |   ) || |   | || |   ) |");
            Console.WriteLine("| |   | |( (   ) )| |   | |");
            Console.WriteLine("| |   ) | \\ \\_/ / | |   ) |");
            Console.WriteLine("| (__/  )  \\   /  | (__/  )");
            Console.WriteLine("(______/    \\_/   (______/");
            Console.WriteLine("--------------------------");
        }
    }
}