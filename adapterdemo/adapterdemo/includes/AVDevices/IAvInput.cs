namespace adapterdemo.includes.AVDevices
{
    /// <summary>
    /// Interface to dictate the methods of an AV Input device
    /// </summary>
    public interface IAvInput
    {
        void Play();
    }
}