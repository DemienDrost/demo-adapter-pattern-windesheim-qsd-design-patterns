﻿using System;
using adapterdemo.includes;
using adapterdemo.includes.AVDevices;

namespace adapterdemo
{
    class Program
    {
        public static void WaitToContinue()
        {
            Console.WriteLine("-------------");
            Console.WriteLine("End of demo, press any key to continue");
            Console.ReadKey();
            Console.Clear();
            PrintMenu();
        }
        public static void PrintMenu()
        {
            Console.WriteLine("Adapter pattern Demo");
            Console.WriteLine("Choose a demo:");
            Console.WriteLine("1) Television using DVD player as input");
            Console.WriteLine("2) Television using Netflix as input (Object Adapter)");
            Console.WriteLine("3) Television using Netflix as input (Class Adapter)");
            Console.WriteLine("-------------");
            Console.WriteLine();
            
            ConsoleKeyInfo option = Console.ReadKey();
            Console.Clear();

            Television TV;
            DvdPlayer DVD;

            ISteamingService service;
            switch (option.Key)
            {
                case ConsoleKey.D1:
                    // Television using DVD player as input
                    Console.WriteLine("[Script] Creating a television");
                    TV = new Television();
                    
                    Console.WriteLine("[Script] Pressing the 'play' button");
                    TV.PlayButton();
                    
                    Console.WriteLine("[Script] Adding the DVD player to the television's AV slot");
                    DVD = new DvdPlayer();
                    TV.ConnectAvInput(DVD);
                    
                    Console.WriteLine("[Script] Pressing the 'play' button");
                    TV.PlayButton();
                    
                    WaitToContinue();
                    break;
                case ConsoleKey.D2:
                    // Television using Netflix as input (Object Adapter)
                    Console.WriteLine("[Script] Creating a television");
                    TV = new Television();
                    
                    Console.WriteLine("[Script] Pressing the 'play' button");
                    TV.PlayButton();
                    
                    Console.WriteLine("[Script] Creating a Chromecast device (Adapter) and adding Netflix (Adaptee) to it");
                    service = new Netflix();
                    Chromecast chromecast = new Chromecast(service);

                    Console.WriteLine("[Script] Adding the Chromecast device to the television's AV slot");
                    TV.ConnectAvInput(chromecast);
                    
                    Console.WriteLine("[Script] Pressing the TV's 'play' button");
                    TV.PlayButton();
                    
                    WaitToContinue();
                    break;
                case ConsoleKey.D3:
                    // Television using Netflix as input (Class Adapter)
                    Console.WriteLine("[Script] Creating a television");
                    TV = new Television();
                    
                    Console.WriteLine("[Script] Pressing the 'play' button");
                    TV.PlayButton();
                    
                    Console.WriteLine("[Script] Creating a AppleTV (Adapter), Netflix is the default and only adapter");
                    AppleTv appleTv = new AppleTv();
                    
                    Console.WriteLine("[Script] Adding the AppleTV device to the television's AV slot");
                    TV.ConnectAvInput(appleTv);
                    
                    Console.WriteLine("[Script] Pressing the TV's 'play' button");
                    TV.PlayButton();
                    
                    WaitToContinue();

                    break;
                default:
                    PrintMenu();
                    break;
            }
        }
        
        static void Main(string[] args)
        {
            PrintMenu();
        }
    }
}