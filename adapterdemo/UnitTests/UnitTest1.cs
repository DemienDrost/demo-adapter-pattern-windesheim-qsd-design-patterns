using System;
using adapterdemo.includes;
using adapterdemo.includes.AVDevices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TelevisionPlayButtonTest()
        {
            Television television = new Television();
            var play = television.PlayButton();
            Assert.IsNull(play);
        }
        
        [TestMethod]
        public void TelevisionDvdPlayerTest()
        {
            Television Television = new Television();
            IAvInput DvdPlayer = new DvdPlayer();
            
            Television.ConnectAvInput(DvdPlayer);
                    
            Assert.AreEqual( DvdPlayer,  Television.PlayButton());
        }
        
        [TestMethod]
        public void TelevisionNetflixTest()
        {
            Television television = new Television();
                    
            var service = new Netflix();
            Chromecast chromecast = new Chromecast(service);

            television.ConnectAvInput(chromecast);
                    
            Assert.AreEqual(chromecast, television.PlayButton());
        }
        
        [TestMethod]
        public void TelevisionNetflixAppleTVObjectTest()
        {
            Television television = new Television();
            AppleTv appleTv = new AppleTv();
                    
            television.ConnectAvInput(appleTv);
                    
            Assert.AreEqual(appleTv, television.PlayButton());
        }
    }
}